import { Record } from "immutable";
import { validateEmail, notEmpty } from "../controllers/validaters";

const initFormState: Form = {
  feilds: {
    name: {
      value: "",
      valid: true,
      validate: notEmpty,
      error: "Please enter your name ",
    },
    email: {
      value: "",
      valid: true,
      validate: validateEmail,
      error: "Please use a valid email",
    },
    message: {
      value: "",
      valid: true,
      validate: notEmpty,
      error: "Please enter your message",
    },
  },
  isFormValided: false,
  messageSend: false,
  formButton: {
    state: "init",
    error:
      "Sorry something when wrong - Messge not send - Could you email me on benlycett@hotmail.com instead?",
  },
};

export interface FormInterface {}
export class FormModel
  extends Record(initFormState)
  implements FormInterface
{
  static fromData(form: Form) {
    return new FormModel(form);
  }
}
