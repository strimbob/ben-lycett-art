import { Record, Map as ImMap } from "immutable";
import { v4 as uuidv4 } from "uuid";
import { pages as pageData } from "./pageData/index";

const defaultView: Page = {
  type: "ABOUT",
  key: "",
  id: "",
  image: null,
  title: "",
  url: "",
  paragraphs: null,
  cards: null,
};

export interface PageInterface {}
export class PageModel
  extends Record(defaultView)
  implements PageInterface
{
  static fromData(page: Page, id: string) {
    return new PageModel({ ...page, key: uuidv4(), id });
  }
}

const loadPages = (pageData: Object) => {
  return Object.entries(pageData).reduce((acc, [key, value]) => {
    return { ...acc, [key]: PageModel.fromData(value as Page, key) };
  }, {});
};

export const Pages: Pages = ImMap({ ...loadPages(pageData) });
