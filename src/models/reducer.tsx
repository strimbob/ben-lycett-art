import * as actionTypes from "../controllers/actionTypes";
import { combineReducers } from "redux";
import { TopBarModel } from "./TopBarModel";
import { Pages } from "./PagesModel";
import { ViewportModel } from "./ViewportModel";
import { FormModel } from "./FormModel";
import { ImagesShape } from "./ImagesShape";

export const PagesReducer = (
  state: Pages = Pages,
  action: Action
): Pages => {
  return state;
};

export const TopBarReducer = (
  state: TopBar = new TopBarModel(),
  action: Action
): TopBar => {
  return state;
};

export const FormReducer = (
  state: Form = new FormModel(),
  action: Action
): Form => {
  const { type, payload } = action;
  switch (type) {
    case actionTypes.FORM_UPDATE:
      return payload;
    case actionTypes.FORM_ClEAR:
      return new FormModel();
    default:
      return state;
  }
};

export const viewportReducer = (
  state: Viewport = new ViewportModel(null),
  action: Action
): Viewport => {
  const { type, payload } = action;
  switch (type) {
    case actionTypes.VIEWPORT_UPDATE:
      return payload;
    default:
      return state;
  }
};

export const ImageReducer = (
  state: Images = new ImagesShape(),
  action: Action
): Images => {
  const { type, payload } = action;
  switch (type) {
    case actionTypes.UPDATE_IMAGE: {
      const image = payload;
      return {
        ...state,
        images: state.images.set(image.url, image.image),
      };
    }
    default:
      return state;
  }
};

export const rootReducer = combineReducers({
  topBar: TopBarReducer,
  pages: PagesReducer,
  viewport: viewportReducer,
  form: FormReducer,
  images: ImageReducer,
});
