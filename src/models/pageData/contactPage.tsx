import { v4 as uuidv4 } from "uuid";

const p1 = "It will be great to hear from you!";

export const contact = () => {
  return {
    type: "CONTACT",
    key: uuidv4(),
    id: uuidv4(),
    image: "https://i.imgur.com/3c7jmGl.jpg",
    title: "Contact",
    url: "/contact",
    paragraphs: [p1],
    cards: null,
  };
};
