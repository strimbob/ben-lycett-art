import { v4 as uuidv4 } from "uuid";
import { images } from "./images";
const p1 =
  "The watercolour paintings that I create use the blackest black as a background colour and then three to four watercolours that are mixed together to create all of the colour variation that are present in the work. I use happenstance  within the process to create the nebula type shapes as a reference to the way that I believe the Universe was created.";
const p2 =
  "When I am painting I try not to author the painting too much as I think the way the colour interacts with itself is beautiful. I like to wait and see where the paint will go and give it a small brushstroke to allow the paint to find it’s own path . As the paint moves under its own stream it interacts in a gorgeous way within the fibres of the cotton in the paper. As the painter I try to encourage this effect as much as possible and these are the results.";
export const home = () => {
  return {
    type: "ABOUT",
    key: uuidv4(),
    id: uuidv4(),
    image: images.fullOfLove,
    title: "Water Colour Painting",
    url: "/",
    paragraphs: [p1, p2],
    cards: null,
  };
};
