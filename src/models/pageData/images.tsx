export const images = {
  lemon: "https://i.imgur.com/sMccuGi.jpg",
  theBow: "https://i.imgur.com/jD6vVHI.jpg",
  sunHat: "https://i.imgur.com/ReFLC2f.jpg",
  theWay: "https://i.imgur.com/Se1faO5.jpg",
  mohawk: "https://i.imgur.com/eDjAdLy.jpg",
  tear: "https://i.imgur.com/QKB87Qz.jpg",
  entity: "https://i.imgur.com/3c7jmGl.jpg",
  fullOfLove: "https://i.imgur.com/YIk0kI3.jpg",
  flame: "https://i.imgur.com/HX0DJJU.jpg",
  process: "https://i.imgur.com/3MvX3oz.jpg",
};
