import { v4 as uuidv4 } from "uuid";

const p1 = "Did not found the page you wished for";
export const unknown = () => {
  return {
    type: "UNKNOWN",
    key: uuidv4(),
    id: uuidv4(),
    image: "https://i.imgur.com/YIk0kI3.jpg",
    title: "could you could try again?",
    url: "/404",
    paragraphs: [p1],
    cards: null,
  };
};
