import { card } from "./cardPage";
import { about } from "./aboutPage";
import { home } from "./homePage";
import { contact } from "./contactPage";
import { unknown } from "./unknown";

export const pages = {
  home: home(),
  about: about(),
  card: card(),
  contact: contact(),
  unknown: unknown(),
};
