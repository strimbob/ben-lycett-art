import { v4 as uuidv4 } from "uuid";
import { images } from "./images";
const p1 =
  "My painting technique is a reflection of me.  I started out studying Photography where I learnt about composition and how light interacts with an object or a texture. I became fascinated with time and with story telling . I went onto study film at Manchester Met and then on to produce film and interactive light experiences for galleries and museums all across the UK. As my work became more and more technical, complex and digital I wanted to create an analog output.";
const p2 = `I take each painting as a performance: partly by me, partly by the materials I use and partly by the technique I use. Each study is painted wet on wet, with no additional layers, so there is a very fine window of time that these paintings are captured within.  The story they tell is an interactive story of the universe.  As the painting plays out on the page , it shares the old and sublime story; of how one thing becomes another over time. How colours change, how elements merge, and the anthropic nature of time.`;
export const about = () => {
  return {
    type: "ABOUT",
    key: uuidv4(),
    id: uuidv4(),
    image: images.process,
    title: "About ",
    url: "/about",
    paragraphs: [p1, p2],
    cards: null,
  };
};
