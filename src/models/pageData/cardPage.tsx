import { v4 as uuidv4 } from "uuid";
import { images } from "./images";
// index: number;
// key: string;
// id: string;
const cards = [
  {
    src: images.lemon,
    title: "Lemon",
    url: "/lemon",
    landscape: false,
    size: "Framed size 40 cm x 60 cm",
    description: ` watercolour on matt black`,
    available: false,
  },
  {
    src: images.theBow,
    title: "The bow",
    url: "/the-bow",
    landscape: false,
    size: "Framed size 40 cm x 60 cm",
    available: false,
  },
  {
    src: images.sunHat,
    title: "Sun hat",
    url: "/sun-hat",
    landscape: true,
    size: "Framed size 40 cm x 60 cm",
    available: true,
  },
  {
    src: images.flame,
    title: "Flame",
    url: "/flame",
    landscape: false,
    size: "Framed size 40 cm x 60 cm",
    available: false,
  },
  {
    src: images.theWay,
    title: "The way",
    url: "/the-way",
    landscape: false,
    size: "Framed size 40 cm x 60 cm",
    available: false,
  },
  {
    src: images.mohawk,
    title: "Mohawk",
    url: "/mohawk",
    landscape: false,
    size: "Framed size 40 cm x 60 cm",
    available: false,
  },
  {
    src: images.tear,
    title: "Tear",
    url: "/tear",
    landscape: true,
    size: "Framed size 40 cm x 60 cm",
    available: false,
  },
  {
    src: images.entity,
    title: "Entity",
    url: "/entity",
    landscape: true,
    size: "Framed size 40 cm x 60 cm",
    available: true,
  },
  {
    src: images.fullOfLove,
    title: "Full of love",
    url: "/full-of-love",
    landscape: true,
    size: "Framed size 40 cm x 60 cm",
    available: false,
  },
];

const addIdToCards = () => {
  return cards.map((card, index) => {
    const image = card.src.replace(".jpg", "");

    const huge = `${image}h.jpg`;
    const large = `${image}l.jpg`;
    const medium = `${image}l.jpg`;

    const imageSizes = { huge, large, medium };
    return {
      ...card,
      index: index,
      key: uuidv4(),
      id: uuidv4(),
      imageSizes,
    };
  });
};

export const card = () => {
  return {
    type: "CARD",
    key: uuidv4(),
    id: uuidv4(),
    image: null,
    title: "Water Colour Painting",
    url: "/painting",
    paragraphs: null,
    cards: addIdToCards(),
  };
};
