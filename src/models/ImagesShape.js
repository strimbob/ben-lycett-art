import { Record, Map } from "immutable";

const defaultState = { images: new Map() };

export class ImagesShape extends Record(defaultState) {}
