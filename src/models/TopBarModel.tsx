import { Record } from "immutable";
import { v4 as uuidv4 } from "uuid";

export const topBar: TopBar = {
  key: uuidv4(),
  src: "https://i.imgur.com/gPiSLUf.jpg",
  title: "Ben Lycett",
  items: [
    { label: "Home", link: "/", id: uuidv4(), key: uuidv4() },
    { label: "About", link: "/about", id: uuidv4(), key: uuidv4() },
    { label: "Painting", link: "/painting", id: uuidv4(), key: uuidv4() },
    { label: "Contact", link: "/contact", id: uuidv4(), key: uuidv4() },
  ],
};

export interface TopBarInterface {}

export class TopBarModel
  extends Record(topBar)
  implements TopBarInterface {}
