type Action = {
  type: string;
  payload: any;
};

type ItemsTop = {
  key: string;
  link: string;
  id: string;
  label: string;
};

type TopBar = {
  key: string;
  title: string;
  src: string;
  items: ItemsTop[];
};

type Dispatch = ({
  type,
  payload,
}: {
  type: string;
  payload: any;
}) => void;

type Card = {
  key: string;
  id: string;
  src: string;
  title: string;
  url: string;
  index: number;
  landscape: boolean;
  imageSizes: { huge: string; large: string; medium: string };
  size: string;
  description: string;
  available: boolean;
};

type ImageSize =
  | "origin"
  | "smallSquare"
  | "bigSquare"
  | "small"
  | "medium"
  | "large"
  | "huge";

type ImageSizes = {
  origin: string;
  smallSquare: string;
  bigSquare: string;
  small: string;
  medium: string;
  large: string;
  huge: string;
};

type Images = { images: Map<K, V> };

type Page = {
  type: "CONTACT" | "ABOUT" | "CARD" | "UNKNOWN";
  key: string;
  id: string;
  image: string | null;
  title: string;
  url: string;
  paragraphs: [string] | null;
  cards: Card[] | null;
};

type Id = string;
type Pages = ImMap<Id, Page>;
type Viewport = {
  width: number;
  height: number;
  deviceType?: "phone" | "computer" | "unkown";
};

type Form = {
  isFormValided: boolean;
  feilds: {
    [key: string]: {
      value: string;
      valid: boolean;
      validate: (value: string) => boolean;
      error: string;
    };
  };
  formButton: {
    state: "init" | "pending" | "sent" | "failed";
    error: string;
  };
  messageSend?: boolean;
};

type RootReducer = {
  topBar: TopBar;
  pages: Pages;
  viewport: Viewport;
  form: Form;
  images: Images;
};
