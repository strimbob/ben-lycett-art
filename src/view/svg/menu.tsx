import React from "react";

export const MenuSvg = () => (
  <svg
    width="23px"
    height="23px"
    viewBox="0 0 33 23"
    version="1.1"
    xmlns="http://www.w3.org/2000/svg"
  >
    <title>Group</title>
    <g
      id="Page-1"
      stroke="none"
      strokeWidth="1"
      fill="none"
      fillRule="evenodd"
      strokeLinecap="square"
    >
      <g
        id="Mobile"
        transform="translate(-12.000000, -14.000000)"
        stroke="#f5f5f5"
        strokeWidth="3"
      >
        <g id="Group" transform="translate(13.000000, 15.000000)">
          <line x1="0.5" y1="0.5" x2="30.5" y2="0.5" id="Line-2"></line>
          <line
            x1="0.5"
            y1="10.5"
            x2="30.5"
            y2="10.5"
            id="Line-2-Copy"
          ></line>
          <line
            x1="0.5"
            y1="20.5"
            x2="30.5"
            y2="20.5"
            id="Line-2-Copy-2"
          ></line>
        </g>
      </g>
    </g>
  </svg>
);
