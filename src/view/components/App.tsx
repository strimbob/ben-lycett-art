import { useRef, useEffect } from "react";
import { MenuBar } from "./2-mod/MenuBar";
import { useSelector, shallowEqual } from "react-redux";
import { useDivSize } from "../../controllers/hooks";
import { Pages } from "./2-mod/Pages";
import { Footer } from "./2-mod/Footer";

export const App = () => {
  const viewport = useSelector(
    (state: RootReducer) => state.viewport,
    shallowEqual
  );
  const ref = useRef<HTMLDivElement>(null);
  useDivSize(ref);

  return (
    <div className="App" ref={ref}>
      <MenuBar viewport={viewport} />
      <div className={"main-content"}>
        <Pages viewport={viewport} />
      </div>
      <Footer viewport={viewport} />
    </div>
  );
};
