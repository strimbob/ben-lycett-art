import { useState, useEffect } from "react";
import { useSelector, shallowEqual, useDispatch } from "react-redux";
import { useLocation } from "wouter";
import { About } from "./About";
import { Contact } from "./Contact";
import { Cards } from "./Cards";
import { images } from "../../../models/pageData/images";
import { updateImage } from "../../../controllers/imagesController";
type Props = {
  viewport: Viewport;
};

export const Pages = (props: Props) => {
  const [location] = useLocation();
  const dispatch = useDispatch();
  const [currentPage, setCurrentPage] = useState(null);
  const pages = useSelector(
    (state: RootReducer) => state.pages,
    shallowEqual
  );

  useEffect(() => {
    Object.entries(images).map(([key, value]) => {
      const loadingImage = `${value}t.jpg`;
      dispatch(updateImage(loadingImage));
    });
  }, []);

  useEffect(() => {
    const pageUrl = location.split("/")[1];
    const page = pages.entrySeq().find((_page: [string, Page]) => {
      const [, page] = _page;
      return page.url === `/${pageUrl}`;
    });

    setCurrentPage(page[1]);
  }, [location, pages]);

  if (!currentPage) return null;
  const _currentPage: Page = currentPage as Page;
  if (_currentPage.type === "ABOUT")
    return <About page={_currentPage} viewport={props.viewport} />;
  if (_currentPage.type === "CONTACT")
    return <Contact page={_currentPage} viewport={props.viewport} />;
  if (_currentPage.type === "CARD")
    return <Cards page={_currentPage} viewport={props.viewport} />;

  return null;
};
