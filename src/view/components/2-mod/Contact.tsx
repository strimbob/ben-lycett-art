import React from "react";
import {
  updateForm,
  validateForm,
  sendForm,
} from "../../../controllers/formController";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import * as ActionTypes from "../../../controllers/actionTypes";

type Props = {
  viewport: Viewport;
  page: Page;
};

export const Contact = (props: Props) => {
  const dispatch = useDispatch();
  const form = useSelector(
    (state: RootReducer) => state.form,
    shallowEqual
  );

  const messageSend = form.formButton.state === "sent" ? true : false;
  const classNameState = form.formButton.state;
  const clear = () => {
    dispatch({ type: ActionTypes.FORM_ClEAR });
  };
  const onChange = (event: any) => {
    const id = event.currentTarget.id as "name" | "email" | "message";
    const value = event.currentTarget.value as string;
    dispatch(updateForm(id, value));
  };

  const validate = (event: React.SyntheticEvent) => {
    const id = event.currentTarget.id as "name" | "email" | "message";
    dispatch(validateForm(id));
  };

  const sendMessage = () => {
    if (form.isFormValided) {
      dispatch(sendForm());
    }
  };

  const { page } = props;

  const buttonText = (state: "init" | "pending" | "sent" | "failed") => {
    switch (state) {
      case "sent":
        return "Message send";
      case "pending":
        return "Message is sending";
      case "failed":
        return "Message Failed";
      case "init":
        return "SEND";
      default:
        return "SEND";
    }
  };
  return (
    <div className={"contact-wrapper"}>
      <div className={"contact"}>
        <div className={"contact-content-wrapper"}>
          <div className={"contact-content"}>
            <div className={"contact-title-wrapper"}>
              <h2 className={"contact-title"}> {page.title}</h2>
            </div>
            <div className={"contact-paragraph-wrapper"}>
              {page.paragraphs?.map((paragraph, index) => {
                return (
                  <p
                    key={`contact-${index}`}
                    className={"contact-paragraph"}
                  >
                    {" "}
                    {paragraph}
                  </p>
                );
              })}
            </div>
          </div>
          <div className={"contact-form"}>
            <input
              id={"name"}
              className={`contact-form-name ${classNameState}`}
              placeholder={"Name"}
              onChange={onChange}
              value={form.feilds.name.value}
              disabled={messageSend}
              onBlur={validate}
            />
            {!form.feilds.name.valid && (
              <div className={"contact-form-error"}>
                {form.feilds.name.error}{" "}
              </div>
            )}
            <input
              id={"email"}
              className={`contact-form-email ${classNameState}`}
              placeholder={"Email"}
              onChange={onChange}
              value={form.feilds.email.value}
              onBlur={validate}
              disabled={messageSend}
            />
            {!form.feilds.email.valid && (
              <div className={"contact-form-error"}>
                {form.feilds.email.error}{" "}
              </div>
            )}
            <textarea
              id={"message"}
              className={`contact-form-message ${classNameState}`}
              onChange={onChange}
              placeholder={"Message"}
              value={form.feilds.message.value}
              onBlur={validate}
              disabled={messageSend}
            />
            {!form.feilds.message.valid && (
              <div className={"contact-form-error"}>
                {form.feilds.message.error}{" "}
              </div>
            )}
            <div
              onClick={sendMessage}
              className={`contact-form-send-wrapper ${classNameState}`}
            >
              <div className={`contact-form-send" ${classNameState}`}>
                {buttonText(classNameState)}
              </div>
            </div>
            {form.formButton.state === "failed" && (
              <div className={"contact-form-error"}>
                {form.formButton.error}
                {" Message not sent "}
              </div>
            )}

            {messageSend && (
              <div className="contact-form-clear-wrapper" onClick={clear}>
                {" "}
                <div className="contact-form-clear"> clear </div>{" "}
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};
