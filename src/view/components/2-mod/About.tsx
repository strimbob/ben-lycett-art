import { Img } from "../1-atom/Img";
type Props = {
  viewport: Viewport;
  page: Page;
};

export const About = (props: Props) => {
  const { page } = props;

  return (
    <div className={"about-wrapper"}>
      <div className={"about"}>
        <Img
          className={"about-image"}
          src={page.image ? page.image : ""}
          alt={"./"}
          imagesSize={"origin"}
        />
        <div className={"about-content-wrapper"}>
          <div className={"about-content"}>
            <div className={"about-title-wrapper"}>
              <h2 className={"about-title"}> {page.title}</h2>
            </div>
            <div className={"about-paragraph-wrapper"}>
              {page.paragraphs?.map((paragraph, index) => {
                return (
                  <p
                    key={`paragraph-${index}`}
                    className={"about-paragraph"}
                  >
                    {" "}
                    {paragraph}
                  </p>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
