import { useEffect, useState } from "react";
import { useLocation } from "wouter";
import { CardPage } from "../1-atom/Card";
type Props = {
  viewport: Viewport;
  page: Page;
};

export const Cards = (props: Props) => {
  const { page } = props;
  const [location, setLocation] = useLocation();
  const [selectedCard, setSelectedCard] = useState<null | Card>(null);

  useEffect(() => {
    const card = page?.cards?.find((card: Card) => {
      return location === `${page.url}${card.url}`;
    });
    setSelectedCard(card ? card : null);
  }, [location, page]);

  const clickHanlder = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    const { id } = event.currentTarget;

    if (id.includes("nav")) {
      if (!selectedCard) return;
      if (!page?.cards) return;
      if (id === "nav-next") {
        const index = selectedCard.index + 1;
        const updateIndex = page.cards.length <= index ? 0 : index;
        setLocation(`${page.url}${page.cards[updateIndex].url}`);
      }
      if (id === "nav-previous") {
        const index = selectedCard.index - 1;
        const updateIndex = index >= 0 ? index : page.cards.length - 1;
        setLocation(`${page.url}${page.cards[updateIndex].url}`);
      }
    } else {
      const found = page?.cards?.find((card) => card.id === id);
      if (!found) return null;
      window.scroll(0, 0);
      setLocation(`${page.url}${found.url}`);
    }
  };

  const cards = page?.cards?.map((__card: Card) => {
    return (
      <CardPage
        key={`card-menu-${__card.key}`}
        card={__card}
        clickHanlder={clickHanlder}
        className={`card-menu`}
        imagesSize={"large"}
      />
    );
  });

  return (
    <div
      className={`cards-wrapper ${
        selectedCard ? "selectedCard" : "cards-menu"
      }`}
    >
      {!selectedCard && <div className={"cards-grid"}>{cards}</div>}
      {selectedCard && (
        <>
          <div className={"card-nav"}>
            <div id={"nav-previous"} onClick={clickHanlder}>
              {" "}
              Previous
            </div>
            <div id={"nav-next"} onClick={clickHanlder}>
              {" "}
              Next
            </div>
          </div>
          <div className={"cards-Page"}>
            <CardPage
              className={"selectedCard"}
              card={selectedCard}
              imagesSize={"origin"}
            />
          </div>
        </>
      )}
    </div>
  );
};
