import React, { useState, useEffect } from "react";
import { useSelector, shallowEqual } from "react-redux";
import { useLocation } from "wouter";
import { MenuSvg } from "../../svg/menu";
type Props = {
  viewport: Viewport;
};

export const MenuBar = (props: Props) => {
  const [location, setLocation] = useLocation();
  const [isOpen, setOpen] = useState(false);
  const [deviceType, setDeviceType] = useState(props.viewport.deviceType);
  const isDetials = location.split("/").length >= 3;

  useEffect(() => {
    setDeviceType(props.viewport.deviceType);
    setOpen(false);
  }, [props.viewport.deviceType]);

  const topBar = useSelector(
    (state: RootReducer) => state.topBar,
    shallowEqual
  );
  const cliclHandler = (event: React.SyntheticEvent) => {
    setOpen(false);
    const { id } = event.currentTarget;

    const foundPage = topBar.items.find((page) => page.id === id);
    if (!foundPage) return;
    setLocation(foundPage.link);
  };

  const menuClick = (event: React.SyntheticEvent) => {
    setOpen((_open) => !_open);
  };

  const menuList = topBar.items.map((item) => {
    const selected = item.link === location ? "selected" : "";
    return (
      <h5
        className={`menu-button ${selected}`}
        id={item.id}
        onClick={cliclHandler}
        key={item.key}
      >
        {item.label}
      </h5>
    );
  });

  return (
    <>
      <div
        className={`menuBar-wrapper ${deviceType} ${
          isDetials ? "details" : ""
        }`}
      >
        <div
          className={`menuBar ${deviceType} ${isDetials ? "details" : ""}`}
        >
          <div
            className={`menu-titles ${deviceType} ${
              isDetials ? "details" : ""
            }`}
          >
            <div className={"menu-title-wrapper"}>
              <h1
                onClick={() => {
                  if (!isDetials) return;
                  const back = location.split("/").slice(0, -1);
                  if (!back) return;
                  const backUrl = back
                    .reduce((acc, value) => {
                      return `${acc}/${value}`;
                    }, "")
                    .replace("/", "");
                  setLocation(backUrl);
                }}
                className={`menu-title  ${deviceType} ${
                  isDetials ? "details" : ""
                }`}
              >
                {" "}
                {isDetials ? "Back" : topBar.title}
              </h1>
            </div>
            {!isDetials && (
              <>
                <div
                  className={`menu-icon-wrapper  ${deviceType}`}
                  onClick={menuClick}
                >
                  <MenuSvg />
                </div>

                <div className={`menu-button-wrapper ${deviceType}`}>
                  {menuList}
                </div>
              </>
            )}
          </div>
        </div>
      </div>
      {isOpen && (
        <div className={`menu-popup-wrapper`}>
          <div className={`menu-popup`}>
            <div className={`menu-button-wrapper`}>{menuList}</div>
          </div>
        </div>
      )}
    </>
  );
};
