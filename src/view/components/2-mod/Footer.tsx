import React from "react";
import { SocialIcon } from "react-social-icons";
import { useLocation } from "wouter";
type Props = {
  viewport: Viewport;
};
export const Footer = (props: Props) => {
  const [location, setLocation] = useLocation();
  const clickHanlder = () => {
    setLocation("/contact");
  };
  return (
    <div className={"footer-wrapper"}>
      <div className={"footer-name-wrapper"}>
        <div className={"footer-name"}>Ben Lycett </div>
        <div className={"footer-name"}>Amsterdam </div>
      </div>
      {location !== "/contact" && (
        <div className={"footer-contact-wrapper"} onClick={clickHanlder}>
          <div className={"footer-contact"}>Contact </div>
        </div>
      )}
      <div className={"footer-instagram-wrapper"}>
        <SocialIcon
          url={"https://www.instagram.com/space.and.colour/"}
          bgColor={"#000"}
        />
      </div>
    </div>
  );
};
