import { Img } from "./Img";
export const CardPage = (props: {
  card: Card;
  showNavigation?: boolean;
  className?: string;
  imagesSize: ImageSize;
  clickHanlder?: (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => void;
}) => {
  const { clickHanlder, card, className } = props;

  return (
    <div
      key={card.key}
      id={card.id}
      className={`card-wrapper ${className}`}
      onClick={clickHanlder && clickHanlder}
    >
      <div className={"card-background"}>
        <Img
          alt={""}
          className={`card-img ${props.card.landscape ? "landscape" : ""}`}
          src={props.card.src}
          imagesSize={props.imagesSize}
        />
      </div>
      <div className={`card-title ${className}`}> {card.title} </div>
    </div>
  );
};
