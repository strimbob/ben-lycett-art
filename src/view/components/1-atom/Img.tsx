import { useEffect, useState } from "react";
import { useDispatch, useSelector, shallowEqual } from "react-redux";
import { updateImage } from "../../../controllers/imagesController";
import { BarLoader } from "react-spinners";

const imageSizesObject: ImageSizes = {
  origin: "",
  smallSquare: "s",
  bigSquare: "b",
  small: "t",
  medium: "m",
  large: "l",
  huge: "h",
};

export const Img = ({
  src,
  className,
  alt,
  imagesSize,
}: {
  src: string;
  className: string;
  alt: string;
  imagesSize: ImageSize;
}) => {
  const [imageLoadingClassName, setImageLoadingClassName] = useState(true);
  const dispatch = useDispatch();
  const _src = src.replace(".jpg", "");
  const requestedImage = `${_src}${imageSizesObject[imagesSize]}.jpg`;
  const loadingImage = `${_src}${imageSizesObject.small}.jpg`;
  const image = useSelector(
    (state: RootReducer) => state?.images?.images.get(requestedImage),
    shallowEqual
  );

  const imageSmall = useSelector(
    (state: RootReducer) => state?.images?.images.get(loadingImage),
    shallowEqual
  );

  useEffect(() => {
    dispatch(updateImage(requestedImage));
    dispatch(updateImage(loadingImage));
  }, [src, dispatch]);

  if (!image) {
    if (!imageSmall) {
      return (
        <div className={`${className} loading`}>
          <BarLoader
            color={"#000"}
            loading
            width={100}
            speedMultiplier={0.3}
          />
        </div>
      );
    }
    return (
      <img
        className={`${className.replace("landscape", "")} "loadingImage" `}
        src={imageSmall}
        alt={alt}
      />
    );
  }

  return (
    <img
      className={`${className} ${
        imageLoadingClassName ? "loadingImage" : ""
      } `}
      src={image}
      alt={imageSmall}
      onLoad={() => {
        setImageLoadingClassName(false);
      }}
    />
  );
};
