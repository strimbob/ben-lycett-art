// export const sendEmail = async () => {
//   const url = "https://send-email-stomp.herokuapp.com/v1/test/email/oksdf";
//   const header = {
//     method: "PUT",
//     headers: { "Access-Control-Allow-Methods": "POST, DELETE, GET, PUT" },
//     body: JSON.stringify({ message: "sdf" }),
//   };
//   return fetch(url, header);
// };

export const sendEmail = async ({
  name,
  email,
  message,
}: {
  name: string;
  email: string;
  message: string;
}) => {
  const url = `https://send-email-stomp.herokuapp.com/v1/ben-lycett/email/${email}`;

  //const url = `http://localhost:4000/v1/test/email/${email}`;

  const header = {
    method: "PUT",
    headers: {
      "Access-Control-Allow-Methods": "POST, DELETE, GET, PUT",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ message: message, name: name }),
  };

  return fetch(url, header);
};
