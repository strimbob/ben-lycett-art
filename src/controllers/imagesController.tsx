import * as ActionTypes from "./actionTypes";
const getImage = ({ url }: { url: string }) => {
  return new Promise(async (resolve, reject) => {
    try {
      const response = await fetch(url);
      const imageBlob = await response.blob();
      const image = URL.createObjectURL(imageBlob);
      resolve(image);
    } catch (error) {
      console.log(error);
      reject(error);
    }
  });
};

//______________________________________________________
export const updateImage =
  (url: string) =>
  async (dispatch: Dispatch, getState: () => RootReducer) => {
    const images = getState().images;
    const image = images?.images.get(url);

    if (image) return;
    try {
      const _image = await getImage({ url });
      dispatch({
        type: ActionTypes.UPDATE_IMAGE,
        payload: { url, image: _image },
      });
    } catch (error) {
      console.error(error);
    }
  };
