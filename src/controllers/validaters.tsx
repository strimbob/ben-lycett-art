export const validateEmail = (email: string) => {
  const regExp =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return new RegExp(regExp).test(email);
};
export const notEmpty = (value: string) => {
  if (!value || value === "" || value === " ") return false;
  return true;
};
