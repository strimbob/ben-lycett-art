import * as ActionTypes from "./actionTypes";
import { FormModel } from "../models/FormModel";
import { sendEmail } from "./api";
//______________________________________________________
export const updateForm =
  (id: "name" | "email" | "message", value: string) =>
  (dispatch: Dispatch, getState: () => RootReducer) => {
    const _form = getState().form;
    const update = _form.feilds[id];
    let valid = true;
    if (!update.valid) {
      valid = update.validate(value);
    }

    const feilds = {
      ..._form.feilds,
      [id]: { ...update, value, valid },
    };

    dispatch({
      type: ActionTypes.FORM_UPDATE,
      payload: new FormModel({ ..._form, feilds }),
    });
  };

//______________________________________________________
export const validateForm =
  (id: "name" | "email" | "message") =>
  (dispatch: Dispatch, getState: () => RootReducer) => {
    const _form = getState().form;
    const update = _form.feilds[id];
    const updateForm = {
      ..._form.feilds,
      [id]: { ...update, valid: update.validate(update.value) },
    };

    const validAll = Object.entries(_form.feilds).reduce(
      (acc, [key, value]) => {
        const valid = value.validate(value.value);
        return { ...acc, [key]: { ...value, valid } };
      },
      _form.feilds
    );

    let isFormValided = Object.entries(validAll).every(([key, value]) => {
      return value.valid;
    });

    dispatch({
      type: ActionTypes.FORM_UPDATE,
      payload: new FormModel({
        ..._form,
        feilds: updateForm,
        isFormValided,
      }),
    });
  };
//______________________________________________________

export const formButtonState =
  (state: "init" | "pending" | "sent" | "failed") =>
  (dispatch: Dispatch, getState: () => RootReducer) => {
    const _form = getState().form;
    const formButton = _form.formButton;
    dispatch({
      type: ActionTypes.FORM_UPDATE,
      payload: new FormModel({
        ..._form,
        feilds: _form.feilds,
        formButton: { ...formButton, state: state },
      }),
    });
  };

export const sendForm =
  () => async (dispatch: Dispatch, getState: () => RootReducer) => {
    const __form = getState().form;

    formButtonState("pending")(dispatch, getState);
    try {
      const sendInfo = {
        name: __form.feilds.name.value,
        email: __form.feilds.email.value,
        message: __form.feilds.message.value,
      };
      const responses = await sendEmail(sendInfo);
      if (responses.status >= 200 && responses.status <= 300) {
        formButtonState("sent")(dispatch, getState);
      } else {
        throw new Error("api error");
      }
    } catch (_error) {
      console.error(_error);
      formButtonState("failed")(dispatch, getState);
    }

    // const update = Object.entries(_form).reduce((acc, [key, value]) => {
    //   const valid = value.validate(value.value);
    //   return { ...acc, [key]: { ...value, valid } };
    // }, _form);

    // let isFormValided = Object.entries(update).every(([key, value]) => {
    //   return value.valid;
    // });
  };
