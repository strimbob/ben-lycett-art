import React from "react";
import ReactDOM from "react-dom";
import "./view/scss/index.scss";
import { App } from "./view/components/App";
import { Provider } from "react-redux";
import { store } from "./models/store";
console.log("welcome....");
ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
